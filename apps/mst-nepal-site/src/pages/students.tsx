import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from 'components/Layout'
import Seo from 'components/Seo'
import FeaturesGrid from 'components/FeaturesGrid'
import JoinUs from 'components/JoinUs'
import HeroSection from 'components/HeroSection'

const Index = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title || `Title`
  const heroSection = data.dataYaml.heroSection
  const featuresSection = data.dataYaml.featuresSection
  const joinUsSection = data.dataYaml.joinUsSection

  return (
    <Layout location={location} title={siteTitle} isFullWidth={false}>
      <Seo title="Home" />
      <section className="section-padding position-relative section--padded-y--md full-width bg-primary-gradient-light">
        <HeroSection data={heroSection} />
      </section>
      <section className="section--padded-t pb-5 full-width ">
        <div className="container">
          <div className="row justify-content-center text-center">
            <div className="col-xl-7 text-center">
              <h5 className="heading text-center h3">
                {featuresSection.title}
              </h5>
              <p className="generic-p mb-5">{featuresSection.subTitle}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="row p-3 justify-content-center">
                <FeaturesGrid list={featuresSection.features} />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="section-padding position-relative section--padded-y--md full-width section--padded-y--md bottom-fold-bg">
        <div className="container">
          <JoinUs data={joinUsSection} />
        </div>
      </section>
    </Layout>
  )
}

export default Index

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    dataYaml(page: { eq: "students" }) {
      id
      heroSection {
        title
        artwork {
          childImageSharp {
            gatsbyImageData(placeholder: DOMINANT_COLOR)
          }
          extension
          publicURL
        }
        description
      }
      featuresSection {
        title
        subTitle
        features {
          title
          img {
            childImageSharp {
              gatsbyImageData(placeholder: DOMINANT_COLOR)
            }
            extension
            publicURL
          }
          description
          isComingSoon
        }
      }
      joinUsSection {
        title
        ctaText
      }
    }
  }
`
