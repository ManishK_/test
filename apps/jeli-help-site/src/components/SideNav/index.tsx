import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { Footer } from '@ap-websites/shared-ui'
import SearchBar from 'components/SearchBar'

const Sidenav = ({ isRootPath }) => {
  const data = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          title
        }
      }
      allMarkdownRemark(sort: { fields: frontmatter___order, order: ASC }) {
        group(field: frontmatter___category) {
          edges {
            node {
              frontmatter {
                title
                showInNav
              }
              fields {
                slug
              }
              id
            }
          }
          fieldValue
        }
      }
    }
  `)
  const posts = data.allMarkdownRemark.group
  const siteTitle = data.site.siteMetadata.title
  // const posts = [...postx].reverse()

  function handleClick() {
    document.getElementById('sidebar').classList.toggle('mobile-sidebar')
    document.getElementById('show-sidebar').classList.toggle('active')
    document.getElementById('hamburger').classList.toggle('hamburger--cross')
  }

  return (
    <>
      <aside className="col-lg-2 d-md-block sidebar" id="sidebar">
        <div className="sidebar-sticky">
          <SearchBar />
          <ul className="nav flex-column">
            <li className="mb-0">
              <Link
                className="color-white"
                to="/"
                itemProp="url"
                activeClassName="active"
              >
                {siteTitle}
              </Link>
            </li>
            <li className="nav-item no-link">
              <div className="py-2 px-3">Table of Content</div>
            </li>
            {posts.map((post) => {
              const edges = post.edges
              const title = post.fieldValue
              return (
                <li className="mb-0" key={post.fieldValue}>
                  <Link
                    className="color-white"
                    to={'/' + title.replace(/\/|\s+/g, '-').toLowerCase() + '/'}
                    itemProp="url"
                    activeClassName="active"
                  >
                    {title}
                  </Link>
                  <ul className="nav flex-column" style={{ listStyle: `none` }}>
                    {edges.map((edge) => {
                      const subtitle = edge.node.frontmatter.title
                      const showInNav = edge.node.frontmatter.showInNav
                      const slug = edge.node.fields.slug
                      return (
                        <li
                          className={`mb-0 ${
                            showInNav !== true ? 'd-none' : ''
                          }`}
                          key={edge.node.id}
                        >
                          <Link
                            className="color-white"
                            to={slug}
                            itemProp="url"
                            activeClassName="active"
                          >
                            {subtitle}
                          </Link>
                        </li>
                      )
                    })}
                  </ul>
                </li>
              )
            })}
          </ul>
          <Footer />
        </div>
      </aside>
      <button
        id="show-sidebar"
        className="show-sidebar btn btn-sm"
        onClick={() => handleClick()}
      >
        <div className="hamburger" id="hamburger">
          <span className="line"></span>
          <span className="line"></span>
          <span className="line"></span>
        </div>
      </button>
    </>
  )
}
export default Sidenav
