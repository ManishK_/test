---
title: Basic Information
date: 2015-02-01T23:46:37.121Z
description:
  In this video, you shall learn the basics of mySecondTeacher, like – how to
  log in, how to change your password, profile picture, and understand the
  features of the Student’s Dashboard.
category: Student
showInNav: true
---

_In this video clip, you shall learn the basics of mySecondTeacher, like – how
to log in, how to change your password, profile picture, and understand the
features of the Student’s Dashboard._

`youtube: wEluyWTMS_Y?rel=0`
