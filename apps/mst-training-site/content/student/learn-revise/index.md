---
title: Learn/Revise
date: '2015-02-02T22:12:03.284Z'
category: Student
showInNav: true
description:
  Learn how to access IVys and eBooks and learn from them, take Mastery
  Assessments & access Mastery Reports
---

_Learn how to access IVys and eBooks and learn from them, take Mastery
Assessments & access Mastery Reports_

`youtube: i9BfMLd6yik?rel=0`
